ActiveRecord::Schema.define do

	create_table "comments", force: true do |t|
	  t.integer 	"post_id"
	  t.text 	"body"
	  t.datetime	"created_at"
	  t.datetime	"updated_at"
	end

	create_table "posts", force:true do |t|
	  t.string	"title"
	  t.text	"body"
	  t.datetime	"created_at"
	  t.datetime	"updated_at"
	end
end
